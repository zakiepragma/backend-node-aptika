// Import modul yang diperlukan
const express = require("express");
const mysql = require("mysql");
const dotenv = require("dotenv");

// Konfigurasi dotenv
dotenv.config();

// Konfigurasi koneksi ke database MySQL
const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
});

// Membuka koneksi ke database MySQL
connection.connect((error) => {
  if (error) {
    console.error("Koneksi ke database gagal: " + error.stack);
    return;
  }
  console.log("Koneksi ke database berhasil dengan ID " + connection.threadId);
});

// Inisialisasi aplikasi Express
const app = express();

const menu = [
  [1, 0, "Beranda", 0],
  [2, 0, "Product", 2],
  [3, 2, "Product 1", 0],
  [4, 2, "Product 2", 1],
  [5, 2, "Product 3", 2],
  [6, 0, "About Us", 1],
  [7, 6, "Contact Us", 0],
  [8, 5, "Product 3.1", 0],
  [9, 2, "Product 4", 3],
];

function printMenu(menu, parentId = 0, level = 0) {
  // Filter array untuk parent menu tertentu
  const children = menu.filter((m) => m[1] === parentId);

  // Urutkan children berdasarkan Ordering
  children.sort((a, b) => a[3] - b[3]);

  // Loop melalui setiap children dan cetak dengan indentasi
  children.forEach((child) => {
    const indent = " ".repeat(level * 2);
    console.log(`${indent}- ${child[2]} (${child[3]})`);

    // Panggil kembali fungsi untuk mencetak children dari setiap child
    printMenu(menu, child[0], level + 1);
  });
}

// Panggil fungsi printMenu untuk mencetak menu
printMenu(menu);

// Konfigurasi route untuk mengakses halaman utama
app.get("/", (req, res) => {
  res.send("Hello World!");
});

// Menjalankan aplikasi pada port tertentu
const port = process.env.PORT || 3002;
app.listen(port, () => {
  console.log("Aplikasi berjalan pada http://localhost:" + port);
});
